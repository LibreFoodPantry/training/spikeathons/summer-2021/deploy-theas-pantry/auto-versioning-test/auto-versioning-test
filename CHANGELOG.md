# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v2.0.0...v1.1.1) (2021-07-21)


### Bug Fixes

* fixed gitignore file ([14c8288](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/14c8288f3fbe63944aa4ee14a3ec8a1309cf5e8a))

## [2.0.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v1.1.0...v2.0.0) (2021-07-21)


### ⚠ BREAKING CHANGES

* add gitignore file

### Features

* add gitignore file ([3440a71](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/3440a71776f8e2d0421c334e0aaa210db56a6fed))

## [1.1.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.3.0...v1.1.0) (2021-07-21)


### Features

* manually  change version number ([a617ae2](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/a617ae2bcb2b6cdf8fa378763d471db4494ececc))

## [0.3.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.2.2...v0.3.0) (2021-07-20)


### ⚠ BREAKING CHANGES

* new stanza in package.json
* new stanza in package.json
* new stanza in package.json

### Features

* new stanza in package.json ([ab594a7](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/ab594a7947011db1de83d67776ab44854cbac332))
* new stanza in package.json ([08f1066](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/08f106673781f3cd022a0aa555c78365d7b4995e))
* new stanza in package.json ([7470e41](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/7470e419a9aae524eb24a1b73e904e7f0ca41571))

### [0.2.2](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.2.1...v0.2.2) (2021-07-14)


### Bug Fixes

* fixed text ([acce3d7](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/acce3d719ebeabfb9f31a43918188dc3b3773094))

### [0.2.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.2.0...v0.2.1) (2021-07-14)


### Features

* new text ([171aba6](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/171aba65bea3909876ce6cd0b340dfdaadba16a4))

## [0.2.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.1.1...v0.2.0) (2021-07-14)


### ⚠ BREAKING CHANGES

* text again

### Features

* new text ([3c75af4](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/3c75af42d91f45c276f7ac41be6e4951dc7aa71d))

### [0.1.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.1.0...v0.1.1) (2021-07-14)

## [0.1.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.0.4...v0.1.0) (2021-07-14)


### ⚠ BREAKING CHANGES

* change text

### Bug Fixes

* change text ([f867a30](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/f867a302e61f8df361d3433a0fc017dfb376f1a5))

### [0.0.4](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.0.3...v0.0.4) (2021-07-14)


### Bug Fixes

* new text ([96a7c94](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/96a7c9453368c6d3cd57a91ec7ef33a808775cfa))

### [0.0.3](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.0.2...v0.0.3) (2021-07-14)

### [0.0.2](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.0.1...v0.0.2) (2021-07-14)

### [0.0.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/compare/v0.0.0...v0.0.1) (2021-07-07)


### Features

* new file ([08d447e](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-test/commit/08d447e9f9773d5b7009619615201af5ca566861))

## 0.0.0 (2021-07-07)
